const os = require('os');
const fs = require('fs');
const CPU = os.cpus();

// Create directory function
function createDirectory(path) {
    fs.mkdir(path, (err) => {
        if (err) {
            console.error(err.message);
            process.exit(0);
        }
        console.log("Successfully created the new folder");
    })
}

// Write contents to file function
function writeFile(path, data) {
    fs.writeFile(path, data, (err) => {
        const pathSplit = path.split('/');

        if (err) {
            console.error(err.message);
            process.exit(0);
        }
        console.log(`Successfully write CPU information into ${pathSplit[pathSplit.length - 1]}`);
    });
}

/**
 * Main section
 * Loop CPU and store data in string
 * 
 */
let cpuDataStr = '';

for (const cpuData of CPU) {
    cpuDataStr += `Model: ${cpuData.model}\nSpeed: ${cpuData.speed}\nUser: ${cpuData.times.user}\nNice: ${cpuData.times.nice}\nSys: ${cpuData.times.sys}\nIdle: ${cpuData.times.idle}\nIRq: ${cpuData.times.irq}\n\n`;
}

fs.access('./text_data', (err) => {
    if (err) {
        createDirectory('./text_data');
    }
    writeFile('./text_data/cpu.txt', cpuDataStr);
});