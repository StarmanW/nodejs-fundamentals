setInterval(() => {
    // Get cpu usage from global variable "process"
    const cpuUsage = process.cpuUsage();

    // Log user and system
    console.log(`User: ${cpuUsage.user} | System: ${cpuUsage.system}`);
}, 1000);