// Required modules
const util = require('util');
const fs = require('fs');

/**
 * The function must be a callback function. Error
 * will be thrown if it is not a function.
 * 
 * For more information, read 
 * https://nodejs.org/dist/latest-v8.x/docs/api/util.html
 * 
 * Promisify fs.readFile function
 */
const readFile = util.promisify(fs.readFile);
const num = 5;

// Util.format demo
console.log(util.format('Number: %d', num));

// Read file contents
readFile('./text_data/cpu.txt', 'utf8')
    .then((data) => {
        console.log(data);
    })
    .catch((err) => {
        console.error(err);
    });